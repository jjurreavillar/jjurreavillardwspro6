<?php
include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos del Programador - Introducir datos</title>
        <link rel="stylesheet" href="css/general.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
        </header>
        <h2>Datos del Programador</h2>
        <div class="contenedor-formulario">
            <form method="POST" action="procesar_datos_programadores.php" >
                <caption>Introduzca los datos del programador</caption>
                <ul class="contenedor-formulario-flex">
                    <li>
                        <label for="id">Identificador:</label>
                        <input type="text" id="id" name="id" placeholder="8419" pattern="\d+" title="Únicamente se permiten números" required />
                    </li>
                    <li>
                        <label for="nombre">Nombre:</label>
                        <input type="text" id="nombre" name="nombre" placeholder="Fulanito Pérez" pattern="[a-zA-Z\s]+" title="El nombre solo puede contener letras y espacios" required />
                    </li>
                    <li>
                        <label for="telefono1">Teléfono:</label>
                        <input type="tel" id="telefono1" name="telefono[]" placeholder="123456789" pattern="\d{9}" title="El teléfono deben ser 9 números" required/>
                    </li>
                    <li>
                        <input type="tel" id="telefono2" name="telefono[]" placeholder="123456789" pattern="\d{9}" title="El teléfono deben ser 9 números"/>
                    </li>
                    <li>
                        <input type="submit" name= "Enviar">
                        <input type="reset" name = "Borrar">
                    </li>
                </ul>
            </form>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>
