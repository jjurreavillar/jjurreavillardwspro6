<?php
header('Content-type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors','1');
include_once("config.php");
require("clases.php");

function escribir_bdd($software)
{
    $sql = new SQL();
    $sql->conectarBDD();

    if ($sql->estaConectadaBDD())
    {
        $sql->consultarBDD("INSERT INTO software(id, nombre, pId) VALUES ('".
$software->getId() . "', '" .
$software->getNombre() . "', '" .
$software->getProgramador() .
"')");
    }
    else
        echo "<h3>No se ha podido conectar a la base de datos. Asegurese de realizar la instalación.</h3><br><br>";
}

function escribir_datos($software)
{
    $f = fopen("software.txt", "at");
    fwrite($f, $software->getId() . ';' . $software->getNombre() . ';' . $software->getProgramador() . PHP_EOL);
    fclose($f);
}

function recoge($campo)
{
    return isset($_POST[$campo]) ? htmlspecialchars(trim(strip_tags($_POST[$campo]))) : "";
}

function recoge_multiple($campo_multiple)
{
    if (isset($_POST[$campo_multiple]))
        foreach ($_POST[$campo_multiple] as $indice => $campo)
            $campo_mult[$indice] = htmlspecialchars(trim(strip_tags($campo)));

    return $campo_mult;
}

$id = recoge("id");
$nombre = recoge("nombre");
$programadorId = recoge("programador");

$software = new Software($id, $nombre, $programadorId);

if (USAR_MYSQL == 0)
    escribir_datos($software);
else
    escribir_bdd($software);

echo "Se han escrito los datos. Se va a redirigir automáticamente a la página principal. Si no se le redirige automáticamente, <a href=\"index.php\">haga clic aquí para volver a la página principal.</a>";

header('Refresh: 10; url=index.php');
?>
