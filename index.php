<?php
include_once("config.php");

if (USAR_MYSQL != 0)
    require("clases.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Proyecto</title>
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" href="css/principal.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
            <h3>Proyecto UD6 DWC</h3>
        </header>
        <div id="contenedor">
            <article>
                <h2>Elija una opción:</h1>
                <div id="subcontenedor">
                    <div id="programador" class="opcion"><a href="programador.php">Programador</a></div>
                    <div id="software" class="opcion"><a href="software.php">Software</a></div>
                    <div id="instalacion" class="opcion"><a href="instalacion.php">Instalación</a></div>
                </div>
            </article>
<?php
if (USAR_MYSQL == 0 && is_file("software.txt"))
{
    echo "<aside><nav><p>Software disponible:</p><ul>";
    $listaSw = file("software.txt");
    
    foreach ($listaSw as $sw)
    {
        $campos = explode(";", $sw);

        echo "<li><a href=\"sw.php?id=$campos[0]\" title=\"$campos[1]\">$campos[1]</a></li>";
    }

    echo "</ul></nav></aside>";
}
else if (USAR_MYSQL != 0)
{
    $sql = new SQL();
    $sql->conectarBDD();

    if ($sql->estaConectadaBDD())
    {
        $consulta = $sql->consultarBDD("SELECT COUNT(*) AS cuenta FROM software");

        foreach($consulta as $valor)
            $cuenta = $valor['cuenta'];
    }

    if (isset($cuenta) && $cuenta > 0)
    {
        echo "<aside><nav><p>Software disponible:</p><ul>";

        $consulta = $sql->consultarBDD("SELECT id,nombre FROM software");

        foreach($consulta as $valor)
        {
            echo "<li><a href=\"sw.php?id=$valor[id]\" title=\"$valor[nombre]\">$valor[nombre]</a></li>";
        }

        echo "</ul></nav></aside>";
    }
}
?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>

