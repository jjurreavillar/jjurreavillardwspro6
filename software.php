<?php
include_once("config.php");
require("clases.php");
if (USAR_MYSQL == 0 && is_file("programadores.txt"))
{
    $contenidos = file("programadores.txt");
    foreach ($contenidos as $linea)
    {
        $arr_programador = explode(";", $linea);
        $arr_telefonos = explode(",", $arr_programador[2]);
        $programadores[] = new Programador($arr_programador[0], $arr_programador[1], $arr_telefonos);
    }
}
else if (USAR_MYSQL != 0)
{
    $sql = new SQL();
    $sql->conectarBDD();

    if ($sql->estaConectadaBDD())
    {
        $query = $sql->consultarBDD("SELECT * FROM programador");

        foreach ($query as $campo)
            $programadores[] = new Programador($campo['id'], $campo['nombre'], $campo['telefono']);
    }
    else
        echo "<h3>No se ha podido conectar a la base de datos. Asegurese de realizar la instalación.</h3><br><br>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Datos del Software - Introducir datos</title>
        <link rel="stylesheet" href="css/general.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
        </header>
        <h2>Datos del Software</h2>
        <div class="contenedor-formulario">
            <form method="POST" action="procesar_datos_software.php" >
                <caption>Introduzca los datos del software</caption>
                <ul class="contenedor-formulario-flex">
                    <li>
                        <label for="id">Identificador:</label>
                        <input type="text" id="id" name="id" placeholder="639" pattern="\d+" title="Únicamente se permiten números" required />
                    </li>
                    <li>
                        <label for="nombre">Nombre:</label>
                        <input type="text" id="nombre" name="nombre" placeholder="Facturación y tal" required />
                    </li>
                    <li>
                        <label for="programador">Programadores:</label>
                        <select id="programador" name="programador">
                            <?php
                            if (isset($programadores))
                                foreach ($programadores as $programador)
                                {
                                    echo "<option value=\"".$programador->getId()."\">";
                                    echo $programador->getNombre()."</option>";
                                }
                            else
                                echo "<option value=\"\">Ningún programador encontrado</option>";
                            ?>
                        </select>
                    </li>
                    <li>
                        <input type="submit" name= "Enviar">
                        <input type="reset" name = "Borrar">
                    </li>
                </ul>
            </form>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>
