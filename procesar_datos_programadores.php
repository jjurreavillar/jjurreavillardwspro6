<?php
header('Content-type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors','1');
include_once("config.php");
require("clases.php");

function escribir_bdd($programador)
{
    $sql = new SQL();
    $sql->conectarBDD();

    if ($sql->estaConectadaBDD())
        $sql->consultarBDD("INSERT INTO programador(id, nombre, telefono) VALUES ('".
$programador->getId() . "', '" .
$programador->getNombre() . "', '" .
implode(",", $programador->getTelefonos()) .
"')");
    else
        echo "<h3>No se ha podido conectar a la base de datos. Asegurese de realizar la instalación.</h3><br><br>";
}

function escribir_datos($programador)
{
    $f = fopen("programadores.txt", "at");
    fwrite($f, $programador->getId() . ';' . $programador->getNombre() . ';' . implode(",", $programador->getTelefonos()) . PHP_EOL);
    fclose($f);
}

function recoge($campo)
{
    return isset($_POST[$campo]) ? htmlspecialchars(trim(strip_tags($_POST[$campo]))) : "";
}

function recoge_multiple($campo_multiple)
{
    if (isset($_POST[$campo_multiple]))
        foreach ($_POST[$campo_multiple] as $indice => $campo)
            $campo_mult[$indice] = htmlspecialchars(trim(strip_tags($campo)));

    return $campo_mult;
}

$id = recoge("id");
$nombre = recoge("nombre");
$telefonos = recoge_multiple("telefono");

$programador = new Programador($id, $nombre, $telefonos);

if (USAR_MYSQL == 0)
    escribir_datos($programador);
else
    escribir_bdd($programador);

echo "Se han escrito los datos. Se va a redirigir automáticamente a la página principal. Si no se le redirige automáticamente, <a href=\"index.php\">haga clic aquí para volver a la página principal.</a>";

header('Refresh: 10; url=index.php');
?>
