<?php
include_once("config.php");

if (USAR_MYSQL != 0)
    require("clases.php");

$subtitulo = "Instalación de la base de datos MySQL";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?=$subtitulo?></title>
        <link rel="stylesheet" href="css/general.css">
    </head>
    <body>
        <header>
            <h1><?=TITULO?></h1>
        </header>
        <h2><?=$subtitulo?></h2>
        <div>
<?php
if (USAR_MYSQL != 0)
{
    SQL::Crear("CREATE DATABASE IF NOT EXISTS ceedcv");
    echo "Creada BDD: ceedcv<br>";
    SQL::Crear("CREATE TABLE IF NOT EXISTS programador(
id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
nombre VARCHAR(50),
telefono VARCHAR(25),
PRIMARY KEY(id)
)", "ceedcv");
    echo "Creada tabla: programador<br>";
    SQL::Crear("CREATE TABLE IF NOT EXISTS software(
id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
pId INTEGER UNSIGNED NOT NULL,
nombre VARCHAR(50),
PRIMARY KEY(id),
FOREIGN KEY (pId) REFERENCES programador(id)
)", "ceedcv");
    echo "Creada tabla: software<br>";
    echo "Creada CAj: software.pId -> programador.id<br>";
}
else
    echo "Instalación no requerida.";
?>
        </div>
        <footer>
            <p><?=FECHA?>, <?=AUTOR?>, <?=CURSO?></p>
            <p><?=EMPRESA?> <a href="doc/Documentacion.pdf">Pulse aquí para leer la documentación.</a></p>
        </footer>
    </body>
</html>
